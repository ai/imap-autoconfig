from flask import Flask, abort, request, make_response, render_template

app = Flask(__name__)


@app.route('/mail/config-v1.1.xml')
def thunderbird_autoconfig_v1():
    # 'request.host' automatically picks the value of the
    # X-Forwarded-Host header if present, Host otherwise.
    http_host = request.host
    if http_host.startswith('autoconfig.'):
        http_host = http_host[11:]
    else:
        abort(400)

    response = make_response(
        render_template('config-v1.1.xml', host=http_host))
    response.headers['Content-Type'] = 'text/xml'
    return response


def create_app():
    app.config.from_envvar('APP_CONFIG', silent=True)
    return app


def main():
    create_app().run()


if __name__ == '__main__':
    main()
