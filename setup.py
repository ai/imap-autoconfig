#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name="imap-autoconfig",
    version="0.1",
    description="Thunderbird autoconfiguration server",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    url="http://git.autistici.org/imap-autoconfig.git",
    install_requires=["Flask"],
    setup_requires=[],
    zip_safe=False,
    packages=find_packages(),
    package_data={"imap_autoconfig": ["templates/*.xml"]},
    entry_points={},
    )

